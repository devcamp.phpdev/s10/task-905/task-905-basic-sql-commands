#Task 905.30 Common clauses 

#Lấy id, first_name, last_name từ bảng customers, sắp xếp theo last_name tăng dần
select id, first_name, last_name from customers order by last_name

#Lấy danh sách các customer_id khác nhau trong bảng orders, sắp xếp giảm dần theo customer_id.
select distinct customer_id from orders order by customer_id desc

#Lấy 5 bản ghi, bắt đầu từ bản ghi số 4, gồm id, first_name, last_name từ employees có job title bắt đầu bằng Sales, sắp xếp theo lastname tăng dần

select id, first_name, last_name from employees where job_title like 'Sales%' order by last_name limit 4,5

#Lấy job  title và số lượng employe theo từng chức danh (job title)
select job_title, count(job_title) from employees group by job_title